(function($){
"use strict";
	
	var tooptip_elem = $('[data-toggle="popover"]');
	tooptip_elem.each(function () {
		jQuery(this).popover();
		$('[data-toggle="popover"].show').popover('show');
	});
	

})(jQuery);