﻿=== HT Mega – Ultimate Addons for WPBakery Page Builder ===
Contributors: htplugins, devitemsllc
Tags: Visual Composer, VC Addons, VC, wpbakery  page builder, wpbakery addons
Requires at least: 3.1
Tested up to: 5.0
Requires PHP: 5.6
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The HTMega is a  WPBakery Page builder (formerly Visual Composer) addons package for WPBakery Page Builder plugin for WordPress.

== Description ==
HTMega is a ultimate addons for WPBakery Page builder (formerly Visual Composer) includes 29+ elements with unlimited variations. HT Mega brings limitless possibilities. Embellish your site with the elements of HT Mega.

You can see the plugin demo here : [Demo](http://demo.wphash.com/htmegavc/)

== Avaible Elements ==
1. [Accordion - 9 Styles ](http://demo.wphash.com/htmegavc/accordion/)
2. [Animated Heading - 7 Styles ](http://demo.wphash.com/htmegavc/animated-heading/)
3. [Block Quote - 5 Styles ](http://demo.wphash.com/htmegavc/blockquote/)
4. [Brand - 7 Styles ](http://demo.wphash.com/htmegavc/brands/)
5. [Business Hours - 5 Styles ](http://demo.wphash.com/htmegavc/business-hours/)
6. [Button - 5 Styles ](http://demo.wphash.com/htmegavc/button/)
7. [Call To Action - 7 Styles ](http://demo.wphash.com/htmegavc/call-to-action/)
8. [Contact Form - 6 Styles ](http://demo.wphash.com/htmegavc/contact-form/)
9. [Countdown - 7 Styles ](http://demo.wphash.com/htmegavc/countdown/)
10. [Counter Up - 6 Styles ](http://demo.wphash.com/htmegavc/counter/)
11. [Dropcaps - 5 Styles ](http://demo.wphash.com/htmegavc/dropcaps/)
12. [Faq - 5 Styles ](http://demo.wphash.com/htmegavc/faq/)
13. [Google Map - 5 Styles ](http://demo.wphash.com/htmegavc/google-map/)
14. [Image Comparison - 5 Styles ](http://demo.wphash.com/htmegavc/image-comparison/)
15. [Image Grid - 5 Styles ](http://demo.wphash.com/htmegavc/images-grid/)
16. [Image Justify - 5 Styles ](http://demo.wphash.com/htmegavc/image-justify/)
17. [Image Magnifire - 5 Styles ](http://demo.wphash.com/htmegavc/image-magnifier/)
18. [Image Masonary - 5 Styles ](http://demo.wphash.com/htmegavc/image-masonry/)
19. [Light Box - 4 Styles ](http://demo.wphash.com/htmegavc/lightbox/)
20. [Mailchimp Subscribe News Letter - 5 Styles ](http://demo.wphash.com/htmegavc/subscribe/)
21. [Popovers - 4 Styles ](http://demo.wphash.com/htmegavc/popovers/)
22. [Pricing Table - 7 Styles ](http://demo.wphash.com/htmegavc/pricing-table/)
23. [Progress Bar - 10 Styles ](http://demo.wphash.com/htmegavc/progress-bar/)
24. [Section Title - 7 Styles ](http://demo.wphash.com/htmegavc/section-title/)
25. [Thumbnails Gallery - 4 Styles ](http://demo.wphash.com/htmegavc/thumbnails-gallery/)
26. [Team Member - 8 Styles ](http://demo.wphash.com/htmegavc/team/)
27. [Testimonial carosel - 5 Styles ](http://demo.wphash.com/htmegavc/testimonial/)
28. [Tool Tip - 4 Styles ](http://demo.wphash.com/htmegavc/tooltips/)
29. [Verticle Timeline - 3 Styles ](http://demo.wphash.com/htmegavc/vertical-timeline/)
30. [Video Player - 4 Styles ](http://demo.wphash.com/htmegavc/video-player/)

== Features: ==
* Fully responsive and mobile ready.
* Unlimited Color and Typography options.
* Retina Ready
* Fully Customizable every element
* Every element enable / disable options panel.
* 877 Google Fonts Supported.
* Cross Browser Compatible.

== Need Help? ==
Is there any feature that you want to get in this plugins? 
Needs assistance to use this plugins? 
Feel free to [Contact us](https://htplugins.com/contact-us/)


== Installation ==
This section describes how to install the HT Mega - Ultimate Addons for WPBakery Page Builder plugin and get it working.

= 1) Install =

= Modern Way: =
1. Go to the WordPress Dashboard "Add New Plugin" section.
2. Search For "HT Mega".
3. Install, then Activate it.

= Old Way: =
1. Unzip (if it is zipped) and Upload `ht-mega-for-wpbakery` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

= 2) Configure =
1. After install and activate the plugin you will get a notice to install WPBakery Page Builder ( If allready install it then do not show notice. ).
2. Install the plugin.
3. HTMega Category will be appear in WPBakery page Editor


== Screenshots ==
1. HTMega Plugin Options
2. Every Addon On/Off Options
3. Every Addon On/Off Options
4. 3rd Party Addon On/Off Options